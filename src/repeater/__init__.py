"""
A class to repeat things.
"""

from threading import Thread
from threading import Event
from time import sleep


class Repeater:
    """
    A class to repeat things.
    """

    def __init__(self, func, interval=1):
        """
        Creates a new repeater.

        :param func: the function to repeat
        :param interval: seconds between call
        """
        self.func = func
        self.interval = interval
        self.thread = Thread(target=self._repeat, daemon=True)
        self.stop_signal = Event()
        self.pause_signal = Event()

    def start(self):
        """
        Starts the repeater in a thread.
        """
        if self.thread.is_alive():
            self.pause_signal.clear()
        else:
            self.thread.start()

    def pause(self):
        """
        Pauses execution of 'func' until start is called.
        """
        self.pause_signal.set()

    def stop(self):
        """
        Stops the repeater and exits the thread.
        """
        self.stop_signal.set()

    def _repeat(self):
        """
        Repeatably calls the 'func' until pause or stop is called.
        """
        while not self.stop_signal.is_set():
            if not self.pause_signal.is_set():
                self.func()
                sleep(self.interval)
