
# Repeater

### A class that repeats things.

Repeater is a project that calls python callables on a specified interval. It executes callables within a separate thread. Execution can be started, paused, and stopped.

### Example

Creating a repeater is very simple. Just pass in the callable and interval (seconds) for which to call.

```python
def hello_world():
    print("Hello World")

repeater = Repeater(hello_world, 1)
repeater.start()
```

This will call the `hello_world` function every second until `pause` or `stop` are called. The thread within a repeater is daemonic causing it to abruptly exit once the calling program exit.

Pausing the repeater continues the thread but stops calling the passed callable. It can be started again by calling `start`.

```python
def hello_world():
    print("Hello World")

repeater = Repeater(hello_world, 1)
repeater.start()
repeater.pause()
repeater.start()
```

Stopping a repeater causes it to exit the thread once the callable has exited appropriately.

```python
def hello_world():
    print("Hello World")

repeater = Repeater(hello_world, 1)
repeater.start()
repeater.stop()
```
