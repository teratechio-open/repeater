"""
Tests the Repeater class.
"""

from time import sleep
from unittest import TestCase

from repeater import Repeater


def hello_world():
    print("Hello World!")


class RepeaterTest(TestCase):
    """
    Tests the Repeater class.
    """

    def test_start(self):
        """
        Tests the start method.
        """
        repeater = Repeater(hello_world, interval=1)
        repeater.start()

        assert repeater.thread.is_alive(), "Thread is not alive!"

    def test_pause(self):
        """
        Tests the pause method.
        """
        repeater = Repeater(hello_world, interval=1)
        repeater.start()
        sleep(5)
        repeater.pause()
        assert repeater.thread.is_alive(),          "Thread is not alive!"
        assert not repeater.stop_signal.is_set(),   "Stop signal is was set!"
        assert repeater.pause_signal.is_set(),      "Pause signal was set!"

    def test_stop(self):
        """
        Tests the stop method.
        """
        repeater = Repeater(hello_world, interval=1)
        repeater.start()
        sleep(5)
        repeater.stop()
        assert repeater.thread.is_alive(),          "Thread is still alive!"
        assert repeater.stop_signal.is_set(),       "Stop signal is was not set!"
        assert not repeater.pause_signal.is_set(),  "Pause signal was set!"
